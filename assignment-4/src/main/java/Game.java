import javax.swing.SwingUtilities;
import javax.sound.sampled.*;
import java.io.*;

public class Game {

    /**
     * main program
     *
     * @param args
     */
    public static void main(String[] args) {
        new Board();
        Sound.bgm.play();
        Sound.bgm.loop();
    }
}