import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.WindowConstants;
import javax.swing.SwingConstants;

class Board extends JFrame {

    /**
     * Instance variables
     */
    private List<Card> cards;
    private Card selectedCard;
    private Card c1;
    private Card c2;
    private Timer t;
    private Timer wait;
    private Timer openTime;
    private int reset = 0;
    private int tries = 0;
    private JFrame mainframe;
    private JLabel labelTries;
    private JLabel labelReset;
    //private JLabel labelCopyright1;
    //private JLabel labelCopyright2;
    private JPanel top;
    private JButton buttonReset;
    private JButton buttonExit;
    private int reveal = 0;
    private static String OS = System.getProperty("os.name").toLowerCase();

    /**
     * Constructor of Board
     */
    public Board() {
        int pairs = 18;
        List<Card> cardsList = new ArrayList<>();
        List<Integer> cardVals = new ArrayList<>();

        for (int i = 0; i < pairs; i++) {
            cardVals.add(i);
            cardVals.add(i);
        }
        Collections.shuffle(cardVals);

        try {
            for (int val : cardVals) {
                String[] chars = {"darthvader", "c3po", "kyloren", "bb8", "luke", "hansolo", "leia", "yoda", "chewbacca", "r2d2", "stormtrooper", "finn", "rey", "bobafett", "obiwankenobi", "poe", "jabba", "padme"};
                String path = null;
                if (OS.contains("mac") || OS.contains("nix") || OS.contains("nux") || OS.contains("aix")) {
                    path = System.getProperty("user.dir").replace('\\', '/') + "/assets/";
                } else if (OS.contains("win")) {
                    path = System.getProperty("user.dir").replace('/', '\\') + "\\assets\\";
                }
                Card c = new Card(val, resizeIcon(new ImageIcon(path + "logo.png")), resizeIcon(new ImageIcon(path + chars[val] + ".png")));
                c.getButton().addActionListener(e -> doTurn(c));
                cardsList.add(c);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,
                    "<html>Error<br>Missing images</html>", "Error!", JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        }

        this.cards = cardsList;

        t = new Timer(750, e -> checkCards());
        t.setRepeats(false);

        createPanel();
    }

    /**
     * method to create game panel
     */
    public void createPanel() {
        mainframe = new JFrame("Match-Pair Memory Game (Star Wars Edition)");
        mainframe.setLayout(new BorderLayout());
        top = new JPanel();
        top.setLayout(new GridLayout(6, 6));
        for (Card c : cards) {
            top.add(c.getButton());
        }
        JPanel mid = new JPanel(new GridLayout(2, 2));
        JPanel bottom = new JPanel(new GridLayout(2, 1));
        JButton buttonReset = new JButton("Reset");
        JButton buttonExit = new JButton("Exit");
        mid.add(buttonReset);
        mid.add(buttonExit);

        labelReset = new JLabel("Number of Reset: " + reset);
        labelTries = new JLabel("Number of Tries: " + tries);
        labelTries.setHorizontalAlignment(SwingConstants.CENTER);
        labelReset.setHorizontalAlignment(SwingConstants.CENTER);
        //labelCopyright1 = new JLabel("Disclaimer: I do NOT own these images nor the songs featured in this game.");
        //labelCopyright2 = new JLabel("All rights belong to it's rightful owner/owner's. No copyright infringement intended.");
        //labelCopyright1.setHorizontalAlignment(SwingConstants.CENTER);
        //labelCopyright2.setHorizontalAlignment(SwingConstants.CENTER);
        mid.add(labelTries);
        mid.add(labelReset);
        //bottom.add(labelCopyright1);
        //bottom.add(labelCopyright2);
        buttonReset.addActionListener(actionPerformed -> reset(cards));
        buttonExit.addActionListener(actionPerformed -> exit());

        mainframe.add(top, BorderLayout.CENTER);
        mainframe.add(mid, BorderLayout.SOUTH);
        //mainframe.add(bottom, BorderLayout.SOUTH);
        mainframe.setPreferredSize(new Dimension(800, 800));
        mainframe.setLocation(500, 250);
        mainframe.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        mainframe.pack();
        mainframe.setVisible(true);
        wait = new Timer(2000, actionPerformed -> revealAllCards());
        wait.start();
        wait.setRepeats(false);
    }

    /**
     * method to implement the exit button
     */
    private void exit() {
        System.exit(0);
    }

    /**
     * method to implement the reset button
     *
     * @param cards, all the cards that will be reset
     */
    private void reset(List<Card> cards) {
        mainframe.remove(top);
        Collections.shuffle(cards);
        JPanel top = new JPanel(new GridLayout(6, 6));
        for (Card card : cards) {
            card.setMatched(false);
            card.release();
            card.getButton().setEnabled(true);
            top.add(card.getButton());
        }
        mainframe.add(top, BorderLayout.CENTER);
        reset++;
        tries = 0;
        labelReset.setText("Number of Reset: " + reset);
        labelTries.setText("Number of Tries: " + tries);
        revealAllCards();
    }

    /**
     * method to reveal all the cards (duh) during the start of the game
     */
    public void revealAllCards() {
        for (Card card : cards) {
            card.pressed();
        }
        top.updateUI();
        openTime = new Timer(2000, actionPerformed -> hideAllCards());
        openTime.setRepeats(false);
        openTime.start();
    }

    /**
     * method to hide all the cards (duh, again) after reveal at the start of the game
     */

    public void hideAllCards() {
        for (Card card : cards) {
            card.release();
        }
        top.updateUI();
    }

    /**
     * method to open up the selected card
     *
     * @param selectedCard, card you selected
     */
    public void doTurn(Card selectedCard) {
        if (c1 == null && c2 == null) {
            c1 = selectedCard;
            c1.pressed();
        }

        if (c1 != null && c1 != selectedCard && c2 == null) {
            c2 = selectedCard;
            c2.pressed();
            t.start();
        }
    }

    /**
     * method to disable and flags button if the id matched.
     */
    public void matchButton() {
        c1.getButton().setEnabled(false);
        c2.getButton().setEnabled(false);
        c1.setMatched(true);
        c2.setMatched(true);
    }

    /**
     * method to check if both cards selected is a match or not
     */
    public void checkCards() {
        if (c1.getId() == c2.getId()) { //match condition
            matchButton();
            JLabel messageLabel = new JLabel("Congratulations, you won the game! | Number of Tries: " + tries);
            if (this.isGameWon()) {
                if (tries < 25) {
                    Sound.win2.play();
                } else if (tries >= 25 && tries < 50) {
                    Sound.win3.play();
                } else if (tries >= 50) {
                    Sound.win.play();
                }
                Object[] choices = {"Quit", "Play Again"};
                int option = JOptionPane.showOptionDialog(null, messageLabel, "Match Pair Memory Game (Star Wars Edition)", JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.PLAIN_MESSAGE, null, choices, choices[1]);
                if (option == 1) {
                    reset(cards);
                } else {
                    System.exit(0);
                }

            }
        } else {
            c1.release();
            c2.release();
            tries++;
            labelTries.setText("Number of Tries: " + tries);
        }
        c1 = null; //reset c1 and c2
        c2 = null;
    }

    /**
     * method to check if the game is done or not
     *
     * @return true if done, and vice versa
     */
    public boolean isGameWon() {
        for (Card c : this.cards) {
            if (!c.getMatched()) {
                return false;
            }
        }
        return true;
    }

    /**
     * method to resize the image to fit the card
     *
     * @param icon
     * @return the resized image
     */
    public static ImageIcon resizeIcon(ImageIcon icon) {
        Image img = icon.getImage();
        Image resizedImage = img.getScaledInstance(135, 120, java.awt.Image.SCALE_SMOOTH);
        return new ImageIcon(resizedImage);
    }
}
