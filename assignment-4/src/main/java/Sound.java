import javax.sound.sampled.*;

public class Sound {

    private static String OS = System.getProperty("os.name").toLowerCase();
    private Clip clip;
    public static Sound bgm = new Sound("music/star-wars-theme-song.wav");
    public static Sound win2 = new Sound("music/jedi-know.wav");
    public static Sound win3 = new Sound("music/forcestrong.wav");
    public static Sound win = new Sound("music/traintheboy.wav");


    /**
     * Constructor of the Sound class
     *
     * @param filename
     */
    public Sound(String filename) {
        if (OS.contains("win")) {
            filename = filename.replace('/', '\\');
        }

        try {
            AudioInputStream ais = AudioSystem.getAudioInputStream(Sound.class.getResource(filename));
            clip = AudioSystem.getClip();
            clip.open(ais);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Accessor of clip variable
     *
     * @return
     */
    public Clip getClip() {
        return clip;
    }

    /**
     * method to play the song
     */
    public void play() {
        try {
            if (clip != null) {
                new Thread() {
                    public void run() {
                        synchronized (clip) {
                            clip.stop();
                            clip.setFramePosition(0);
                            clip.start();
                        }
                    }
                }.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * method to loop the song
     */
    public void loop() {
        try {
            if (clip != null) {
                new Thread() {
                    public void run() {
                        synchronized (clip) {
                            clip.stop();
                            clip.setFramePosition(0);
                            clip.loop(Clip.LOOP_CONTINUOUSLY);
                        }
                    }
                }.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
