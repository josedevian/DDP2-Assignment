public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms
    WildCat cat;
    TrainCar next;

    public TrainCar(WildCat cat) {
        this(cat, null);
    }

    public TrainCar(WildCat cat, TrainCar next) {
        this.cat = cat;
        this.next = next;
    }
    // membuat method untuk menghitung total berat kucing-kucing
    public double computeTotalWeight() {
        if(this.next == null){
            return (EMPTY_WEIGHT + this.cat.weight);
        }
        else{
            return (EMPTY_WEIGHT + this.cat.weight) + this.next.computeTotalWeight();
        }
    }    
    // membuat method untuk menghitung total indeks massa kucing-kucing
    public double computeTotalMassIndex() {
        if(this.next == null){
            return this.cat.computeMassIndex();
        }
        else{
            return this.cat.computeMassIndex() + this.next.computeTotalMassIndex();
        }
    }
    // membuat method untuk mencetak penumpang dari kucing-kucing yang berangkat
    public void printCar() {
        if(this.next == null){
            System.out.printf("(%s)", this.cat.name);
        }
        else{
            System.out.printf("(%s)--", this.cat.name);
            this.next.printCar();
        }
    }
}

// Jose Devian Hibono | 1706039603 | DDP-2-B | NN |
