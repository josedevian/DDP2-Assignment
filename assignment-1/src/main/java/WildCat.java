import java.lang.Math;
public class WildCat {

    // some instance variables with their encapsulation
    String name;
    double weight; // In kilograms
    double length; // In centimeters

    // here below is the constructor of the class
    public WildCat(String name, double weight, double length) {
        this.name = name;
        this.weight = weight;
        this.length = length;
    }
    // here to create a method for computing the Mass Index of the Cat using the BMI calculation
    public double computeMassIndex() {
        double bodyMassIndex = weight / ((length/100)*(length/100));
        return bodyMassIndex;
    }
}

// Jose Devian Hibono | 1706039603 | DDP-2-B | NN |
