import java.util.Scanner;
import java.util.ArrayList;
import java.text.DecimalFormat;

public class A1Station {

	private static final double THRESHOLD = 250; // in kilograms
	private static ArrayList<TrainCar> listGerbong = new ArrayList<TrainCar>(); // membuat arrayList

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		// membuat variable kosong untuk menghitung berat dan massa rata-rata
		double weightTemp = 0;
		double massTemp = 0;
		// meminta jumlah inputan ke user
		int n = Integer.parseInt(input.nextLine());
		for(int i=0;i<n;i++){
			String masukan = input.nextLine();
			String[] tmp = masukan.split(",");
			String name = tmp[0];
			double weight = Double.parseDouble(tmp[1]);
			double length = Double.parseDouble(tmp[2]);
			WildCat kargo = new WildCat(name, weight, length);
			TrainCar gerbong = new TrainCar(kargo);
			listGerbong.add(gerbong);
			// menambahkan massa dan berat sementara untuk menghitung berat dan massa rata-rata 
			weightTemp += gerbong.computeTotalWeight();
			massTemp += gerbong.computeTotalMassIndex();
			if(weightTemp >= THRESHOLD){
				int akhir = (listGerbong.size() - 1);
				for(int a = akhir; a > 0; a--){
					listGerbong.get(a).next = listGerbong.get(a - 1);
				}
				System.out.println("The train departs to Javari Park");
				System.out.print("[LOCO]<--");
				listGerbong.get(akhir).printCar();
				System.out.print("\n");
				int jumlah = listGerbong.size();
				double avgMass = (massTemp/jumlah);
				// menghitung dan membuat rata-rata massa kucing
				System.out.printf("Average mass index of all cats: %.2f \n", avgMass);
				if(avgMass < 18.5){
					System.out.println("In average, the cats in the train are *underweight*");
				}
				else if(avgMass >= 18.5 && avgMass < 25){
					System.out.println("In average, the cats in the train are *normal*");
				}
				else if(avgMass >= 25 && avgMass < 30){
					System.out.println("In average, the cats in the train are *overweight*");
				}
				else if(avgMass >= 30){
					System.out.println("In average, the cats in the train are *obese*");
				}
				// mengosongkan variabel sementara dan arrayList sementara agar dapat digunakan lagi untuk kereta selanjutnya
				listGerbong.clear();
				weightTemp = 0;
				massTemp = 0;
			}
		}
		// membuat kondisi di mana ketika sisa dari inputan masih ada, maka kucing-kucing tersebut akan diberangkatkan segera
		if(listGerbong.size() > 0){
			int akhir = (listGerbong.size() - 1);
			for(int a = akhir; a > 0; a--){
				listGerbong.get(a).next = listGerbong.get(a - 1);
			}
			System.out.println("The train departs to Javari Park");
			System.out.print("[LOCO]<--");
			listGerbong.get(akhir).printCar();
			System.out.print("\n");
			int jumlah = listGerbong.size();
			double avgMass = (massTemp/jumlah);
			System.out.printf("Average mass index of all cats: %.2f \n", avgMass);
			if(avgMass < 18.5){
				System.out.println("In average, the cats in the train are *underweight*");
			}
			else if(avgMass >= 18.5 && avgMass < 25){
				System.out.println("In average, the cats in the train are *normal*");
			}
			else if(avgMass >= 25 && avgMass < 30){
				System.out.println("In average, the cats in the train are *overweight*");
			}
			else if(avgMass >= 30){
				System.out.println("In average, the cats in the train are *obese*");
			}
			listGerbong.clear();
			weightTemp = 0;
			massTemp = 0;
		}
	}
}

// Jose Devian Hibono | 1706039603 | DDP-2-B | NN |
