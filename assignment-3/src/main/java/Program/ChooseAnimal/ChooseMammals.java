package Program.ChooseAnimal;

public class ChooseMammals {
    public static void chooseMammals() {
        System.out.println();
        System.out.println("--Explore the Mammals--");
        System.out.println("1. Hamster\n" +
                "2. Lion\n" +
                "3. Cat\n" +
                "4. Whale\n");
        System.out.println("Please choose your preferred animals (type the number): ");
    }
}
