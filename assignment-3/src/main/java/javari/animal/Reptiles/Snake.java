package javari.animal.Reptiles;

import javari.animal.Condition;
import javari.animal.Gender;

public abstract class Snake extends Reptiles {

    public Snake(Integer id, String type, String name, Gender gender, double length,
                 double weight, Condition condition, String status) {
        super(id, type, name, length, weight, condition, status);
    }

    @Override
    protected abstract boolean specificCondition() {
        return isTame();
    }

    public static void chooseAttraction() {
        System.out.println();
        System.out.println("--Snake--");
        System.out.println("Attraction by Snake");
        System.out.println("1. Dancing Animals");
        System.out.println("2. Counting Masters");
        System.out.println("Please choose your preferred attractions (type the number):");
    }
}