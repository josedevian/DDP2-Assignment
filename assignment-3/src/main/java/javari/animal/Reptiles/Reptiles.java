package javari.animal.Reptiles;

import javari.animal.Animal;
import javari.animal.Condition;
import javari.animal.Gender;

public abstract class Reptiles extends Animal{

    private boolean isTame;

    public Reptiles(Integer id, String type, String name, Gender gender, double length,
                   double weight, Condition condition, String status){
        super(id, type, name, length, weight, condition);
        if(status.equals("tame")){return this.isTame = true;}
        else{return this.isTame = false;}
    }

    public boolean isTame() {
        return isTame;
    }
}