package javari.animal.Mammals;

import javari.animal.Condition;
import javari.animal.Gender;

public abstract class Hamster extends Mammals {

    public Hamster(Integer id, String type, String name, Gender gender, double length,
                double weight, Condition condition, String status) {
        super(id, type, name, length, weight, condition, status);
    }

    @Override
    protected abstract boolean specificCondition() {
        return isPregnant()
    }

    public static void chooseAttraction() {
        System.out.println();
        System.out.println("--Hamster--");
        System.out.println("Attraction by Hamster");
        System.out.println("1. Dancing Animals");
        System.out.println("2. Counting Masters");
        System.out.println("3. Passionate Coders");
        System.out.println("Please choose your preferred attractions (type the number):");
    }
}