package javari.animal.Mammals;

import javari.animal.Condition;
import javari.animal.Gender;

public abstract class Cat extends Mammals {

    public Cat(Integer id, String type, String name, Gender gender, double length,
                 double weight, Condition condition, String status) {
        super(id, type, name, length, weight, condition, status);
    }

    @Override
    protected abstract boolean specificCondition() {
        return isPregnant();
    }

    public static void chooseAttraction() {
        System.out.println();
        System.out.println("--Cat--");
        System.out.println("Attraction by Cat");
        System.out.println("1. Dancing Animals");
        System.out.println("2. Passionate Coders");
        System.out.println("Please choose your preferred attractions (type the number):");
    }
}