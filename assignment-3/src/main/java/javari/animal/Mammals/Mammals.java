package javari.animal.Mammals;

import javari.animal.Animal;
import javari.animal.Body;
import javari.animal.Condition;
import javari.animal.Gender;

public abstract class Mammals extends Animal{

    private boolean isPregnant;

    public Mammals(Integer id, String type, String name, Gender gender, double length,
                double weight, Condition condition, String status){
        super(id, type, name, length, weight, condition);
        if(status.equals("pregnant")){return this.isPregnant = true;}
        else{return this.isPregnant = false;}
    }

    public boolean isPregnant() {
        return isPregnant;
    }
}