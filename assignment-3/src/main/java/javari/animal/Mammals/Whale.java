package javari.animal.Mammals;

import javari.animal.Condition;
import javari.animal.Gender;

public abstract class Whale extends Mammals {

    public Whale(Integer id, String type, String name, Gender gender, double length,
                  double weight, Condition condition, String status) {
        super(id, type, name, length, weight, condition, status);
    }

    @Override
    protected abstract boolean specificCondition() {
        return isPregnant();
    }

    public static void chooseAttraction() {
        System.out.println();
        System.out.println("--Whale--");
        System.out.println("Attraction by Whale");
        System.out.println("1. Circle of Fire");
        System.out.println("2. Counting Masters");
        System.out.println("Please choose your preferred attractions (type the number):");
    }
}