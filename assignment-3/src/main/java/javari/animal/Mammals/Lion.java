package javari.animal.Mammals;

import javari.animal.Condition;
import javari.animal.Gender;

public abstract class Lion extends Mammals {

    public Lion(Integer id, String type, String name, Gender gender, double length,
                 double weight, Condition condition, String status) {
        super(id, type, name, length, weight, condition, status);
    }

    @Override
    protected abstract boolean specificCondition() {
        return isPregnant() && isFemale();
    }

    private boolean isFemale() {
        if (this.getGender().equals(Gender.MALE)) {
            return false;
        } else {
            return true;
        }
    }

    public static void chooseAttraction() {
        System.out.println();
        System.out.println("--Lion--");
        System.out.println("Attraction by Lion");
        System.out.println("1. Circle of Fire");
        System.out.println("Please choose your preferred attractions (type the number):");
    }
}