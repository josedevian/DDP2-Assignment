package javari.animal.Aves;

import javari.animal.Condition;
import javari.animal.Gender;

public abstract class Parrots extends Aves{

    public Parrots(Integer id, String type, String name, Gender gender, double length,
                   double weight, Condition condition, String laying){
        super(id, type, name, length, weight, condition, laying);
    }

    @Override
    protected abstract boolean specificCondition(){
        return isLayingEgg();
    }

    public static void chooseAttraction(){
        System.out.println();
        System.out.println("--Parrot--");
        System.out.println("Attraction by Parrot");
        System.out.println("1. Dancing Animals");
        System.out.println("2. Counting Masters");
        System.out.println("Please choose your preferred attractions (type the number):");
    }
}