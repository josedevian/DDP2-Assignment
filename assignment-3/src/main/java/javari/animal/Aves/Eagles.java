package javari.animal.Aves;

import javari.animal.Condition;
import javari.animal.Gender;

public abstract class Eagles extends Aves {

    public Eagles(Integer id, String type, String name, Gender gender, double length,
                  double weight, Condition condition, String laying) {
        super(id, type, name, length, weight, condition, laying);
    }

    @Override
    protected abstract boolean specificCondition() {
        return isLayingEgg();
    }

    public static void chooseAttraction() {
        System.out.println();
        System.out.println("--Eagle--");
        System.out.println("Attraction by Eagle");
        System.out.println("1. Circle of Fire");
        System.out.println("Please choose your preferred attractions (type the number):");
    }
}