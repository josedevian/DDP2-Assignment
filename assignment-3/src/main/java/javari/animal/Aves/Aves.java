package javari.animal.Aves;

import javari.animal.Animal;
import javari.animal.Body;
import javari.animal.Condition;
import javari.animal.Gender;

public abstract class Aves extends Animal{

    private boolean isLayingEgg;

    public Aves(Integer id, String type, String name, Gender gender, double length,
                double weight, Condition condition, String laying){
        super(id, type, name, length, weight, condition);
        if(laying.equals("laying")){return this.isLayingEgg = true;}
        else{return this.isLayingEgg = false;}
    }

    public boolean isLayingEgg() {
        return isLayingEgg;
    }
}