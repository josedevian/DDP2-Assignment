package javari.park;

import java.util.ArrayList;
import java.util.List;

public class Visitor implements Registration{

    private static int id = 0;
    private String name;
    private List<SelectedAttraction> listAtraksi = new ArrayList<>();

    public Visitor(String name){
        this.name = name;
        id += 1;
    }

    @Override
    public int getRegistrationId(){
        return id;
    }

    @Override
    public String getVisitorName(){
        return name;
    }

    @Override
    public List<SelectedAttraction> getListAtraksi(){
        return listAtraksi;
    }

    @Override
    public boolean addSelectedAttraction(SelectedAtrraction selected){
        listAtraksi.add(selected);
        return true;
    }
}