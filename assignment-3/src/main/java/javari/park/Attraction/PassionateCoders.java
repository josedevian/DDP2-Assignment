package javari.park.Attraction;

import javari.animal.Animal;
import javari.park.SelectedAttraction;

import java.util.ArrayList;
import java.util.List;

public class PassionateCoders implements SelectedAttraction{

    private String name = "Passionate Coders";
    private List<Animal> listPerformers = new ArrayList<>();
    private String type;

    public PassionateCoders(String type){
        this.type = type;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public List<Animal> getListPerformers() {
        return listPerformers;
    }

    @Override
    public boolean addPerformer(Animal performer){
        if(performer.isShowable()){
            listPerformers.add(performer);
            return true;
        }
        else{
            return false;
        }
    }
}