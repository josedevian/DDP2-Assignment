package javari.park.Attraction;

import javari.animal.Animal;
import javari.park.SelectedAttraction;

import java.util.ArrayList;
import java.util.List;

public class CircleOfFire implements SelectedAttraction{

    private String name = "Circle of Fire";
    private List<Animal> listPerformers = new ArrayList<>();
    private String type;

    public CircleOfFire(String type){
        this.type = type;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public List<Animal> getListPerformers() {
        return listPerformers;
    }

    @Override
    public boolean addPerformer(Animal performer){
        if(performer.isShowable()){
            listPerformers.add(performer);
            return true;
        }
        else{
            return false;
        }
    }
}