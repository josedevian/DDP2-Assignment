import java.util.ArrayList;

//membuat class Cages
public class Cages{
    private ArrayList<Animals> list1 = new ArrayList<>();
    private Animals list2[][];
    private Animals list2New[][];
    private int size;

    public Cages(ArrayList<Animals> list1) {
        this.list1 = list1;
    }

    public ArrayList<Animals> getList1() {
        return list1;
    }

    public void setList1(ArrayList<Animals> list1) {
        this.list1 = list1;
    }
    //membuat method untuk meng-arrange
    public void arrange(){
        int i = 0;
        if(list1.size() > ((list1.size()/3)*3)){
            size = list1.size() / 3;
            i = 3 - (list1.size() - size*3);
        }
        else{
            size = list1.size() / 3 - 1;
        }
        list2 = new Animals[3][size+1];
        int j = 0;

        if(size == 0){
            for(int a=0; a<list1.size(); a++){
                list2[a][0] = list1.get(a);
            }
        }
        else{
            for(int x=0; x<3; x++){
                for(int y=0; y<size+1; y++){
                    if(y==size && x < i){
                        break;
                    }
                    else{
                        list2[x][y] = list1.get(j);
                        j++;
                    }
                }
            }
        }
        System.out.println("location: " + list2[0][0].getCagePlace());
        printAnimal(list2, size);
        System.out.println();
    }
    //membuat method untuk print Animal beserta levelnya
    public void printAnimal(Animals listAnimal[][], int size){
        for(int c = 2; c>= 0; c--){
            System.out.print("level " + (c+1) + ": ");
            for(int d = 0; d<size+1; d++){
                if(listAnimal[c][d] == null){
                    continue;
                }
                else{
                    System.out.print(listAnimal[c][d].getName() + "(" + listAnimal[c][d].getLength() + " - " + listAnimal[c][d].getCageCode() + "), ");
                }
            }
            System.out.println();
        }
    }
    //membuat method untuk arrange ulang
    public void rearrange(){
        list2New = new Animals[3][size+1];
        int temp1 = 1;
        for(int i = 2; i>=0; i--){
            int temp2 = size;
            if(temp1 == -1){
                temp1 = 2;
            }
            for(int j = 0; j<size+1; j++){
                list2New[i][j] = list2[temp1][temp2];
                temp2--;
            }
            temp1--;
        }
        System.out.println("After rearrangement...");
        printAnimal(list2New, size);
        System.out.println();
    }
}