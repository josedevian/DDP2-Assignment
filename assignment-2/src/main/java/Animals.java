public class Animals{
    private String name;
    private int length;
    private char cageCode;
    private String cagePlace;

    //constructor cuy
    public Animals(String name, int length) {
        this.name = name;
        this.length = length;
        this.cageCode = cageCode;
        this.cagePlace = cagePlace;
    }
    //setters getters cuy
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public char getCageCode() {
        return cageCode;
    }

    public void setCageCode(char cageCode) {
        this.cageCode = cageCode;
    }

    public String getCagePlace() {
        return cagePlace;
    }

    public void setCagePlace(String cagePlace) {
        this.cagePlace = cagePlace;
    }
}