//membuat class Eagle dan method interaksi
public class Eagle extends Wild{
    public Eagle(String name, int length){
        super(name, length);
    }
    public void action(int number){
        if(number == 1){
            this.flyEagle();
        }
        else{
            System.out.println("You do nothing...\nBack to the office!\n");
        }
    }
    public void flyEagle(){
        System.out.println(this.getName() + " makes a voice: kwaakk…\nYou hurt!\nBack to the office!\n");
    }
}