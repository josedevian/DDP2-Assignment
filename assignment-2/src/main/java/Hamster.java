import java.util.Scanner;

//membuat kelas Hamster dan method interaksi
public class Hamster extends Pet{
    public Hamster(String name, int length){
        super(name, length);
    }
    public void action(int number){
        if(number == 1){
            this.gnaw();
        }
        else if(number == 2){
            this.runTheWheel();
        }
        else{
            System.out.println("You do nothing...\nBack to the office!\n");
        }
    }
    public void gnaw(){
        System.out.println(this.getName() + " says: ngkkrit.. ngkkrrriiit");
        System.out.println("Back to the office!\n");
    }
    public void runTheWheel(){
        System.out.println(this.getName() + " says: trrr... trrr...");
        System.out.println("Back to the office!\n");
    }
}