import java.util.Random;
import java.util.Scanner;

//membuat kelas Cat dan method interaksi
public class Cat extends Pet{
    public Cat(String name, int length) {
        super(name, length);
    }
    public void action(int number){
        if(number == 1){
            this.brushTheFur();
        }
        else if(number == 2){
            this.cuddle();
        }
        else{
            System.out.println("You do nothing...\nBack to the office!\n");
        }
    }
    public void cuddle(){
        String[] catVoice = {"Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"};
        Random rand = new Random();
        int index = rand.nextInt(catVoice.length);
        System.out.println(this.getName() + " says: " + catVoice[index]);
        System.out.println("Back to the office!\n");
    }

    public void brushTheFur(){
        System.out.println("Time to clean "+ this.getName() + "'s fur");
        System.out.println(this.getName() + " says: Nyaaan...");
        System.out.println("Back to the office!\n");
    }
}
