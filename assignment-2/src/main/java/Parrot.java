import java.util.Scanner;

//membuat kelas Parrot dan method interaksi
public class Parrot extends Pet{
    public Parrot(String name, int length) {
        super(name, length);
    }

    public void action(int number) {
        if (number == 1) {
            this.flyParrot();
        } else if (number == 2) {
            this.doConversation();
        } else {
            System.out.println(this.getName() + " says: HM?");
            System.out.println("Back to the office!\n");
        }
    }

    public void flyParrot() {
        System.out.println("Parrot " + this.getName() + " flies!");
        System.out.println(this.getName() + " makes a voice: FLYYYY…..");
        System.out.println("Back to the office!");
    }

    public void doConversation() {
        Scanner input = new Scanner(System.in);
        String voice = input.nextLine();
        System.out.print("You say: " + voice);
        System.out.println(this.getName() + " says: " + voice.toUpperCase() + "\nBack to the office!\n");
    }
}