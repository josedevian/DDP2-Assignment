import java.util.Scanner;

//membuat class Wild untuk binatang tidak bersahabat
public class Wild extends Animals{

    public Wild(String name, int length){
        super(name, length);
        this.setCagePlace("outdoor");

        if(this.getLength() < 75){this.setCageCode('A');}
        else if(this.getLength() >= 75 && this.getLength() < 90){this.setCageCode('B');}
        else if(this.getLength() >= 90){this.setCageCode('C');}
    }
}