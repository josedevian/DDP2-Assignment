import java.util.Random;
import java.util.Scanner;

//membuat kelas Pet untuk binatang bersahabat
public class Pet extends Animals{

    public Pet(String name, int length){
        super(name, length);
        this.setCagePlace("indoor");

        if(this.getLength() < 45){this.setCageCode('A');}
        else if(this.getLength() >= 45 && this.getLength() < 60){this.setCageCode('B');}
        else if(this.getLength() >= 60){this.setCageCode('C');}
    }
}
