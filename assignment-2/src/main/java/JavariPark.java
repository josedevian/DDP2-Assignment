import java.util.Scanner;
import java.util.ArrayList;

//membuat main class
public class JavariPark {
    public static ArrayList<Animals> listCat = new ArrayList<>();
    public static ArrayList<Animals> listEagle = new ArrayList<>();
    public static ArrayList<Animals> listHamster = new ArrayList<>();
    public static ArrayList<Animals> listParrot = new ArrayList<>();
    public static ArrayList<Animals> listLion = new ArrayList<>();

    public static void main(String[] args) {
        String[] listAnimalType = {"cat", "lion", "eagle", "parrot", "hamster"};
        int[] listCount = new int[5];
        Scanner input = new Scanner(System.in);
        System.out.println("Welcome to Javari Park!\nInput the number of animals");
        for (int i = 0; i < 5; i++) {
            System.out.print(listAnimalType[i] + ": ");
            int count = Integer.parseInt(input.nextLine());
            listCount[i] = count;
            if (count > 0) {
                JavariPark park = new JavariPark();
                park.information(listAnimalType[i]);
            }
        }
        System.out.println("Animals have been successfully recorded!");
        JavariPark park2 = new JavariPark();
        park2.printArrange();
        System.out.println("NUMBER OF ANIMALS:\ncat:" + listCat.size() + "\nlion:" + listLion.size() + "\nparrot:" + listParrot.size() + "\neagle:" + listEagle.size() + "\nhamster:" + listHamster.size() + "\n");
        System.out.println("=============================================");
        park2.visit();
    }
    //membuat method information untuk menampung informasi binatang yang ada
    public void information(String type) {
        System.out.println("Provide the information of " + type + "(s):");
        Scanner inputAnimal = new Scanner(System.in);
        String[] list1 = inputAnimal.nextLine().split(",");

        for (String temp : list1) {
            String[] list2 = temp.split("\\|");
            if (type.equals("cat")) {
                Cat kucing = new Cat(list2[0], Integer.parseInt(list2[1]));
                listCat.add(kucing);
            } else if (type.equals("lion")) {
                Lion singa = new Lion(list2[0], Integer.parseInt(list2[1]));
                listLion.add(singa);
            } else if (type.equals("eagle")) {
                Eagle elang = new Eagle(list2[0], Integer.parseInt(list2[1]));
                listEagle.add(elang);
            } else if (type.equals("parrot")) {
                Parrot beo = new Parrot(list2[0], Integer.parseInt(list2[1]));
                listParrot.add(beo);
            } else if (type.equals("hamster")) {
                Hamster rodent = new Hamster(list2[0], Integer.parseInt(list2[1]));
                listHamster.add(rodent);
            }
        }
    }
    //membuat method ketika untuk berkunjung
    public void visit() {
        while (true) {
            System.out.println("Which animal you want to visit?\n" + "(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: Exit)");
            Scanner input = new Scanner(System.in);
            String temp = input.nextLine();
            boolean availability = false;

            if (temp.equals("1")) {
                System.out.print("Mention the name of cat you want to visit: ");
                String tempName = input.nextLine();
                for (Animals tempAnimal : listCat) {
                    if (tempName.equals(tempAnimal.getName())) {
                        System.out.println("You are visiting " + tempName + " (cat) now, what would you like to do?\n1: Brush the fur 2: Cuddle");
                        int tempNum = Integer.parseInt(input.nextLine());
                        ((Cat)tempAnimal).action(tempNum);
                        availability = true;
                    }
                }
                if (!availability) {
                    System.out.println("There is no cat with that name! Back to the office!\n");
                }
            } else if (temp.equals("2")) {
                System.out.print("Mention the name of eagle you want to visit: ");
                String tempName = input.nextLine();
                for (Animals tempAnimal : listEagle) {
                    if (tempName.equals(tempAnimal.getName())) {
                        System.out.println("You are visiting " + tempName + "(eagle) now, what would you like to do?\n1: Order to fly");
                        int tempNum = Integer.parseInt(input.nextLine());
                        ((Eagle)tempAnimal).action(tempNum);
                        availability = true;
                    }
                }
                if (!availability) {
                    System.out.println("There is no eagle with that name! Back to the office!");
                }
            } else if (temp.equals("3")) {
                System.out.print("Mention the name of hamster you want to visit: ");
                String tempName = input.nextLine();
                for (Animals tempAnimal : listHamster) {
                    if (tempName.equals(tempAnimal.getName())) {
                        System.out.println("You are visiting " + tempName + "(hamster) now, what would you like to do?\n1: See it gnawing 2: Order to run in the hamster wheel");
                        int tempNum = Integer.parseInt(input.nextLine());
                        ((Hamster)tempAnimal).action(tempNum);
                        availability = true;
                    }
                }
                if (!availability) {
                    System.out.println("There is no hamster with that name! Back to the office!");
                }
            } else if (temp.equals("4")) {
                System.out.print("Mention the name of parrot you want to visit: ");
                String tempName = input.nextLine();
                for (Animals tempAnimal : listParrot) {
                    if (tempName.equals(tempAnimal.getName())) {
                        System.out.println("You are visiting " + tempName + " (parrot) now, what would you like to do?\n1: Order to fly 2: Do conversation\n");
                        int tempNum = Integer.parseInt(input.nextLine());
                        ((Parrot)tempAnimal).action(tempNum);
                        availability = true;
                    }
                }
                if (!availability) {
                    System.out.println("There is no parrot with that name! Back to the office!");
                }
            } else if (temp.equals("5")) {
                System.out.print("Mention the name of lion you want to visit: ");
                String tempName = input.nextLine();
                for (Animals tempAnimal : listLion) {
                    if (tempName.equals(tempAnimal.getName())) {
                        System.out.println("You are visiting " + tempName + " (lion) now, what would you like to do?\n1: See it hunting 2: Brush the mane 3: Disturb it\n");
                        int tempNum = Integer.parseInt(input.nextLine());
                        ((Lion)tempAnimal).action(tempNum);
                        availability = true;
                    }
                }
                if (!availability) {
                    System.out.println("There is no lion with that name! Back to the office!");
                }
            } else {
                break;
            }
        }
    }
    //membuat method printArrange
    public void printArrange() {
        System.out.println("\n=============================================\nCage arrangement:");
        if (listCat.size() != 0) {
            Cages catCage = new Cages(listCat);
            catCage.arrange();
            catCage.rearrange();
        }if (listLion.size() != 0) {
            Cages lionCage = new Cages(listLion);
            lionCage.arrange();
            lionCage.rearrange();
        }if (listEagle.size() != 0) {
            Cages eagleCage = new Cages(listEagle);
            eagleCage.arrange();
            eagleCage.rearrange();

        }if (listParrot.size() != 0) {
            Cages parrotCage = new Cages(listParrot);
            parrotCage.arrange();
            parrotCage.rearrange();
        }
        if (listHamster.size() != 0) {
            Cages hamsterCage = new Cages(listHamster);
            hamsterCage.arrange();
            hamsterCage.rearrange();
        }

    }
}