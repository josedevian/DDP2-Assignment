import java.util.Scanner;

//membuat class Lion dan method interaksi
public class Lion extends Wild{
    public Lion(String name, int length){
        super(name, length);
    }
    public void action(int number){
        if(number == 1){
            this.hunting();
        }
        else if(number == 2){
            this.brush();
        }
        else if(number == 3){
            this.disturb();
        }
        else{
            System.out.println("You do nothing...\nBack to the office!\n");
        }
    }
    public void hunting(){
        System.out.println("Lion is hunting..");
        System.out.println(this.getName() + " makes a voice: err....!\nBack to the office!\n");
    }
    public void brush(){
        System.out.println("Clean the lion’s mane..");
        System.out.println(this.getName() + " makes a voice: Hauhhmm!\nBack to the office!\n");
    }
    public void disturb(){
        System.out.println(this.getName() + " makes a voice: HAAHUM!!\nBack to the office!\n");
    }
}